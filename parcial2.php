<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Matriz Cuadrada</title>
</head>
<body>
<?php

// Función para generar una matriz cuadrada con números aleatorios
function generarMatrizCuadrada($tamanio) {
    $matriz = array();
    for ($i = 0; $i < $tamanio; $i++) {
        $fila = array();
        for ($j = 0; $j < $tamanio; $j++) {
            $fila[] = rand(0, 9);
        }
        $matriz[] = $fila;
    }
    return $matriz;
}

// Función para imprimir la matriz y calcular la suma de la diagonal principal
function imprimirMatrizYDiagonal($matriz) {
    $sumaDiagonal = 0;
    echo '<table border="1">';
    foreach ($matriz as $fila) {
        echo '<tr>';
        foreach ($fila as $valor) {
            echo '<td>' . $valor . '</td>';
        }
        echo '</tr>';
        $sumaDiagonal += $fila[key($fila)];
        next($fila);
    }
    echo '</table>';
    return $sumaDiagonal;
}

// Función principal que ejecuta el proceso
function generarMatricesHastaSumaDiagonal($min, $max) {
    define('TAMANIO_MATRIZ', 5); // Tamaño de la matriz (filas y columnas)
    
    do {
        $matriz = generarMatrizCuadrada(TAMANIO_MATRIZ);
        $sumaDiagonal = imprimirMatrizYDiagonal($matriz);
        echo '<p>Suma de la diagonal principal: ' . $sumaDiagonal . '</p>';
    } while ($sumaDiagonal < $min || $sumaDiagonal > $max);
    
    echo '<p>La suma de la diagonal principal está entre ' . $min . ' y ' . $max . '. Fin del script.</p>';
}

generarMatricesHastaSumaDiagonal(10, 15);

?>
</body>
</html>
